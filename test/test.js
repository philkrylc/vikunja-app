#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const email = process.env.EMAIL;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function (done) {
        if (!process.env.EMAIL) return done(new Error('EMAIL env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));

        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
        done();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerUser() {
        await browser.get(`https://${app.fqdn}/register`);
        await waitForElement(By.id('register-submit'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('email')).sendKeys(email);
        await browser.findElement(By.id('password1')).sendKeys(password);
        await browser.findElement(By.id('password2')).sendKeys(password);
        await browser.findElement(By.id('registerform')).submit();
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//div[contains(text(), "Please confirm your email address.")]')).submit();
    }

    async function verifyUser() {
        const cmd = `cloudron exec --app ${app.fqdn} -- bash -c "PGPASSWORD=\\\${CLOUDRON_POSTGRESQL_PASSWORD} psql -h \\\${CLOUDRON_POSTGRESQL_HOST} -p \\\${CLOUDRON_POSTGRESQL_PORT} -U \\\${CLOUDRON_POSTGRESQL_USERNAME} -d \\\${CLOUDRON_POSTGRESQL_DATABASE} -Atc \\"SELECT email_confirm_token FROM users\\""`;
        const confirmToken = execSync(cmd, { encoding: 'utf8' }).trim();
        console.log(`confirmation token is ${confirmToken}`);
        await browser.get(`https://${app.fqdn}/?userEmailConfirm=${confirmToken}`);
        await waitForElement(By.xpath('//div[contains(text(), "You successfully confirmed")]'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.xpath('//h2[text()="Login"]'));

        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();

        await waitForElement(By.xpath('//h3[contains(text(), "Current tasks")]'));
    }

    async function createLabel() {
        await browser.get(`https://${app.fqdn}/labels/new`);
        await waitForElement(By.xpath('//h3[text()="Create a new label"]'));

        await browser.findElement(By.xpath('//input[contains(@placeholder, "The label title")]')).sendKeys('CloudronLabel');
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[contains(text(), "CloudronLabel")]'));
    }

    async function checkLabel() {
        await browser.get(`https://${app.fqdn}/labels`);
        await waitForElement(By.xpath('//a[contains(text(), "CloudronLabel")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await browser.findElement(By.xpath(`//button/span[text()="${username}"]`)).click();
        await waitForElement(By.xpath('//a[contains(text(), "Logout")]'));
        await browser.findElement(By.xpath('//a[contains(text(), "Logout")]')).click();
        await waitForElement(By.xpath('//h2[text()="Login"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can register new user', registerUser);
    it('can verify user', verifyUser);
    it('can login', login);
    it('can create label', createLabel);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can register new user', registerUser);
    it('can verify user', verifyUser);
    it('can login', login);
    it('can create label', createLabel);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
