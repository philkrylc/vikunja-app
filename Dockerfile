FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

# Those ARGs need to be adapted in order to setup a specific version.
ARG VERSIONAPI="0.17.0"
ARG VERSIONAPIDL="https://dl.vikunja.io/api/0.17.0/vikunja-v0.17.0-linux-amd64-full"
ARG VERSIONFRONTEND=vikunja-frontend-0.17.0

# Create code folder
RUN mkdir -p /app/code/api /app/code/frontend
WORKDIR /app/code

### Vikunja-API
# Download and unzip vikunja api
RUN wget ${VERSIONAPIDL} -O /tmp/vikunja-api.zip && \
    unzip /tmp/vikunja-api.zip -d /app/code/api && \
    ln -s /app/code/api/vikunja-v${VERSIONAPI}-linux-amd64 /app/code/api/vikunja-api

# Create a symbolic link so config is where the app expects it
RUN ln -s /app/data/config.yml /app/code/api/config.yml

### Vikunja Frontend
# Download and unzip vikunja frontend
RUN wget https://dl.vikunja.io/frontend/${VERSIONFRONTEND}.zip -O /tmp/${VERSIONFRONTEND}.zip && \
    unzip /tmp/${VERSIONFRONTEND}.zip -d /app/code/frontend

# add nginx config
USER root
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD vikunja-nginx /etc/nginx/sites-enabled/vikunja

ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf

ADD supervisor/ /etc/supervisor/conf.d/

# setup log paths
RUN sed -e 's,^logfile=.*$,logfile=/run/vikunja/supervisord.log,' -i /etc/supervisor/supervisord.conf

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
